const { Map } = require("immutable-ext")

const Sum = x =>
  ({
    x,
    concat: ({x: y}) =>
      Sum(x + y),
    inspect: () =>
      `Sum(${x})`
  })

Sum.empty = () => Sum(0) // empty is Monoid, 0 is neutral element

const All = x =>
  ({
    x,
    concat: ({x: y}) =>
      All(x && y),
    inspect: () =>
      `All(${x})`
  })

All.empty = () => All(true)

const First = x =>
  ({
    x,
    concat: _ =>
      First(x),
    inspect: () =>
      `First(${x})`
  })

// First.empty cant be defined 

////////////////////////////////////////////////////////////////////////
const sum = xs =>
  xs.reduce((acc, x) => acc + x, 0)

const all = xs =>
  xs.reduce((acc, x) => acc && x, true)

const first = xs =>
  xs.reduce((acc, x) => acc)

console.log(first[1,2,3,4]) // 1
console.log(first[]) // exception, unsafe

