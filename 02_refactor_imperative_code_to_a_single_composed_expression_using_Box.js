const Box = x => 
  ({
    map: f => Box(f(x)),
    fold: f => f(x),
    inspect: () => `Box(${x})`
  })

// const moneyToFloat = str =>
//   parseFloat(str.replace(/\$/g, ''))

const moneyToFloat = str =>
  Box(str)
    .map(s => str.replace(/\$/g, ''))
    .map(r => parseFloat(r))

// const percentToFloat = str => {
//   const replaced = str.replace(/\%/g, '')
//   const number = parseFloat(replaced)
//   return number * 0.01
// }

const percentToFloat = str => 
  Box(str.replace(/\%/g, ''))
    .map(replaced => parseFloat(replaced))
    .map(number => number * 0.01)


// const applyDiscound = (price, discount) => {
//   const cost = moneyToFloat(price)
//   const savings = percentToFloat(discount)
//   return cost - cost * savings
// }

const applyDiscound = (price, discount) => 
  moneyToFloat(price)
    .fold(cost =>
      percentToFloat(discount)
        .fold(saving => 
          cost - cost * saving))

const result = applyDiscound('$5.00', '20%')
console.log(result)


