const { List } = require("immutable-ext")

const Sum = x =>
  ({
    x,
    concat: ({x: y}) => Sum(x + y),
  })

Sum.empty = () => Sum(0)

//-------
const Product = x =>
  ({
    x,
    concat: ({x: y}) => Product(x * y)
  })

Product.empty = () => Product(1)

//-------
const Any = x =>
  ({
    x,
    concat: ({x: y}) => Any(x || y)
  })

Any.empty = () => Any(false)

//-------
const All = x =>
  ({
    x,
    concat: ({x: y}) => All(x || y)
  })

All.empty = () => All(true)

//-------
const Max = x =>
  ({
    x,
    concat: ({x: y}) => Max(x > y ? x : y)
  })

Max.empty = () => Max(-Infinite)

//-------
const Min = x =>
  ({
    x,
    concat: ({x: y}) => Min(x < y ? x : y)
  })

Min.empty = () => Min(Infinite)

//-------
const Right = x =>
  ({
    fold: (f, g) => g(x),
    map: f => Right(f(x)),
    concat: o =>
      o.fold(e => Left(e),
        r => Right(x.concat(r))),
    inspect: x => `Right(${x})`
  })

const Left = x =>
  ({
    fold: (f, g) => f(x),
    map: f => Left(x),
    concat: o => Left(o)
  })

const fromNullable = x =>
  x != null ? Right(x) : Left(null)

//---------
const stats1 = List.of({page: 'Home', views: 40},
                       {page: 'About', views: 10},
                       {page: 'Blog', views: 4})

console.log(
  stats1.foldMap(x => 
    fromNullable(x.views).map(Sum), Right(Sum(0)))
    .fold(_ => _, res => res)
)

const stats2 = List.of({page: 'Home', views: 40},
                       {page: 'About', views: 10},
                       {page: 'Blog', views: null})

console.log(
  stats2.foldMap(x => 
    fromNullable(x.views).map(Sum), Right(Sum(0)))
    .fold(_ => _, res => res)
)
